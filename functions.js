const previewHTML = document.getElementById("preview");
const progressHTML = document.getElementById("progress");
const resultHTML = document.getElementById("result");

async function analizeImage(source) {
  progressHTML.innerHTML = `<div class="loader"></div>`;
  let result = await Tesseract.recognize(source).progress();
  progressHTML.innerHTML = null;
  resultHTML.innerHTML = `<b>Result: ${result.text}</b>`;
}
