const clearButton = document.getElementById("clear");
const canvas = document.getElementById("myCanvas");
const analiceButton = document.getElementById("analice");
const selectColor = document.getElementById("selectColor");
const selectWidth = document.getElementById("selectWidth");
const ctx = canvas.getContext("2d");
let coord = { x: 0, y: 0 };

(() => {
  document.addEventListener("mousedown", start);
  document.addEventListener("mouseup", stop);

  function reposition(event) {
    coord.x = event.clientX - canvas.offsetLeft;
    coord.y = event.clientY - canvas.offsetTop;
  }

  function start(event) {
    document.addEventListener("mousemove", draw);
    reposition(event);
  }

  function stop() {
    document.removeEventListener("mousemove", draw);
  }

  function draw(event) {
    ctx.beginPath();
    ctx.lineWidth = selectWidth.value;
    ctx.lineCap = "round";
    ctx.strokeStyle = selectColor.value;
    ctx.moveTo(coord.x, coord.y);
    reposition(event);
    ctx.lineTo(coord.x, coord.y);
    ctx.stroke();
  }

  clearButton.addEventListener("click", () => {
    // Use the identity matrix while clearing the canvas
    ctx.setTransform(1, 0, 0, 1, 0, 0);
    ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
  });

  analiceButton.addEventListener("click", () => {
    var img = new Image();
    img.crossOrigin = "Anonymous";
    let canvas = document.getElementById("myCanvas");
    dataURL = canvas.toDataURL();
    img.src = dataURL;
    img.onload = (e) => {
      analizeImage(dataURL);
    };
  });
})();
